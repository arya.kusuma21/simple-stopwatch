let interval;
let remainingTime;

const addZero = (number) => (number < 10 ? '0' + number : number);

const startTimer = (duration, display) => {
    let timer = duration;
    interval = setInterval(() => {
        const hours = Math.floor(timer / 3600);
        const minutes = Math.floor((timer % 3600) / 60);
        const seconds = timer % 60;

        display.textContent = `${addZero(hours)}:${addZero(minutes)}:${addZero(seconds)}`;

        if (--timer < 0) {
            clearInterval(interval);
            display.textContent = "00:00:00";
            document.body.style.backgroundColor = 'red';
            document.getElementById('pause').hidden = true;
            playMusic();
        }

        remainingTime = timer;
    }, 1000);
};
const initializeTimer = () => {
    const hourInput = parseInt(document.getElementById('hour').value, 10) || 0;
    const minuteInput = parseInt(document.getElementById('minute').value, 10) || 0;
    const secondInput = parseInt(document.getElementById('second').value, 10) || 0;
    const display = document.getElementById('display');
    const duration = (hourInput * 3600) + (minuteInput * 60) + secondInput;
    startTimer(duration, display);
}
const playMusic = () => {
    const audio = document.getElementById('audio');
    console.log(audio.value);
    if (audio.value === '1') {
        samsung.play();
    } else if (audio.value === '2') {
        punjabi.play();
    }
}

const stopMusic = () => {
    const audioElements = document.querySelectorAll('audio');
    audioElements.forEach(audio => {
        audio.pause();
        audio.currentTime = 0;
    })
}

document.querySelectorAll('#hour, #minute, #second').forEach(input => {
    input.addEventListener('input', () => {
        const hour = document.getElementById('hour').value;
        const minute = document.getElementById('minute').value;
        const second = document.getElementById('second').value;
        document.getElementById('start').disabled = !(hour || minute || second);
    });
});

start.addEventListener('click', () => {
    const reset = document.getElementById('reset');
    const pause = document.getElementById('pause');
    reset.hidden = false;
    pause.hidden = false;
    start.hidden = true;
    initializeTimer();
})

reset.addEventListener('click', () => {
    clearInterval(interval);
    document.getElementById('display').innerText = '00:00:00';
    document.getElementById('hour').value = '0';
    document.getElementById('minute').value = '0';
    document.getElementById('second').value = '0';
    const start = document.getElementById('start');
    const pause = document.getElementById('pause');
    start.hidden = false;
    pause.hidden = true;
    reset.hidden = true;
    document.body.style.backgroundColor = 'white';
    stopMusic();
})

pause.addEventListener('click', () => {
    clearInterval(interval);
    document.getElementById('pause').hidden = true;
    document.getElementById('resume').hidden = false;
})

resume.addEventListener('click', () => {
    const duration = remainingTime;
    startTimer(duration, document.getElementById('display'));
    document.getElementById('pause').hidden = false;
    document.getElementById('resume').hidden = true;
})

